package com.idexx.search.engine.exception;

import org.springframework.http.HttpStatus;

public class ErrorResponse {
	private HttpStatus status;
	private String message;
	
	public ErrorResponse(SearchException exception) {
		super();
		this.status = exception.getStatus();
		this.message = exception.getMessage();
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	
}
