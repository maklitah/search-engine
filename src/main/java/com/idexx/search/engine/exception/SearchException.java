package com.idexx.search.engine.exception;

import org.springframework.http.HttpStatus;

public class SearchException extends Exception {
	
	private static final long serialVersionUID = -6289233518972665374L;
	
	private HttpStatus status;
	private String message;
	
	public SearchException(HttpStatus status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
