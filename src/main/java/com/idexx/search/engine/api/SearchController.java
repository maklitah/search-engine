package com.idexx.search.engine.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.idexx.search.engine.dto.SearchResultDTO;
import com.idexx.search.engine.exception.SearchException;
import com.idexx.search.engine.services.SearchService;

@RestController
@RequestMapping("/api/search")
public class SearchController {

	@Autowired
    @Qualifier("googleSearchService")
	private SearchService googleSearchService;
	
	@Autowired
    @Qualifier("iTunesSearchService")
	private SearchService iTunesSearchService;
	
	@GetMapping(path = "/{query}")
    public List<SearchResultDTO> get(@PathVariable("query") String query) throws SearchException {
		List<SearchResultDTO> results = new ArrayList<SearchResultDTO>();
		results.addAll(googleSearchService.getResults(query));
		results.addAll(iTunesSearchService.getResults(query));
		
		return results;
    }
}
