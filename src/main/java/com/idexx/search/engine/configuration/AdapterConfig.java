package com.idexx.search.engine.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.idexx.search.engine.adapters.Adapter;
import com.idexx.search.engine.adapters.GoogleAdapter;
import com.idexx.search.engine.adapters.ITunesAdapter;
import com.idexx.search.engine.dto.adapters.responses.GoogleResponse;
import com.idexx.search.engine.dto.adapters.responses.ITunesResponse;

@Configuration
public class AdapterConfig {

    @Bean
    public Adapter<GoogleResponse> googleAdapter(RestTemplate restTemplate, @Value("${adapter.google.url}") String googleUrl, @Value("${adapter.google.limit}") String limit) {
        return new GoogleAdapter(restTemplate, googleUrl, limit);
    }

    @Bean
    public Adapter<ITunesResponse> iTunesAdapter(RestTemplate restTemplate, @Value("${adapter.itunes.url}") String itunesUrl, @Value("${adapter.itunes.limit}") String limit) {
        return new ITunesAdapter(restTemplate, itunesUrl, limit);
    }
}
