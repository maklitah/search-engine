package com.idexx.search.engine.advice;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.idexx.search.engine.exception.ErrorResponse;
import com.idexx.search.engine.exception.SearchException;

@ControllerAdvice
public class SearchExceptionHandler {
	
	@ExceptionHandler(value = { SearchException.class})
	protected ResponseEntity<ErrorResponse> handleConflict(SearchException ex, WebRequest request) {
        return new ResponseEntity<ErrorResponse>(new ErrorResponse(ex), ex.getStatus());
    }
}
