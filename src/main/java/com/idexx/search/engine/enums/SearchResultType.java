package com.idexx.search.engine.enums;

public enum SearchResultType {
	BOOK,
	ALBUM;
}
