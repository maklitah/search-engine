package com.idexx.search.engine.converters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.idexx.search.engine.dto.SearchResultDTO;
import com.idexx.search.engine.dto.adapters.responses.ITunesResponse;
import com.idexx.search.engine.enums.SearchResultType;

@Component
public class ITunesResponseConverter implements Converter<ITunesResponse, List<SearchResultDTO>>{

	@Override
	public List<SearchResultDTO> convert(ITunesResponse source) {
		if (source == null) {
			return new ArrayList<>();
		}
		
		List<SearchResultDTO> searchResultDTOs = new ArrayList<SearchResultDTO>();		
		for (ITunesResponse.Album album: source.getResults()) {
			SearchResultDTO searchResult = new SearchResultDTO();
			searchResult.setTitle(album.getCollectionName());
			searchResult.setCreators(Collections.singletonList(album.getArtistName()));
			searchResult.setSearchResultType(SearchResultType.ALBUM);
			
			searchResultDTOs.add(searchResult);
		}
		
		return searchResultDTOs;
	}

}
