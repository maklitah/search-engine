package com.idexx.search.engine.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.idexx.search.engine.dto.SearchResultDTO;
import com.idexx.search.engine.dto.adapters.responses.GoogleResponse;
import com.idexx.search.engine.enums.SearchResultType;

@Component
public class GoogleResponseConverter implements Converter<GoogleResponse, List<SearchResultDTO>>{

	@Override
	public List<SearchResultDTO> convert(GoogleResponse source) {
		if (source == null || source.getItems() == null) {
			return new ArrayList<>();
		}
		
		List<SearchResultDTO> searchResultDTOs = new ArrayList<SearchResultDTO>();		
		for (GoogleResponse.Book book: source.getItems()) {
			GoogleResponse.Book.VolumeInfo info =  book.getVolumeInfo();
			SearchResultDTO searchResult = new SearchResultDTO();
			searchResult.setTitle(info.getTitle());
			searchResult.setCreators(info.getAuthors());
			searchResult.setSearchResultType(SearchResultType.BOOK);
			
			searchResultDTOs.add(searchResult);
		}
		
		return searchResultDTOs;
	}
}
