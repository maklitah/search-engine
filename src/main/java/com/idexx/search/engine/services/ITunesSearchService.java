package com.idexx.search.engine.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idexx.search.engine.adapters.Adapter;
import com.idexx.search.engine.converters.ITunesResponseConverter;
import com.idexx.search.engine.dto.SearchResultDTO;
import com.idexx.search.engine.dto.adapters.responses.ITunesResponse;
import com.idexx.search.engine.exception.SearchException;

@Service("iTunesSearchService")
public class ITunesSearchService implements SearchService {

	@Autowired
	private Adapter<ITunesResponse> iTunesAdapter;

	@Autowired
	private ITunesResponseConverter iTunesResponseConverter;
	
	@Override
	public List<SearchResultDTO> getResults(String query) throws SearchException {
		ITunesResponse iTunesResponse = iTunesAdapter.get(query);
		return iTunesResponseConverter.convert(iTunesResponse);
	}
}
