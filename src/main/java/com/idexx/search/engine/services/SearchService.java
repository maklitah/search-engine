package com.idexx.search.engine.services;

import java.util.List;

import com.idexx.search.engine.dto.SearchResultDTO;
import com.idexx.search.engine.exception.SearchException;

public interface SearchService {
	
	List<SearchResultDTO> getResults(String query) throws SearchException;
}
