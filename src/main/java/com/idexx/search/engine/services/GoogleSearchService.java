package com.idexx.search.engine.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idexx.search.engine.adapters.Adapter;
import com.idexx.search.engine.converters.GoogleResponseConverter;
import com.idexx.search.engine.dto.SearchResultDTO;
import com.idexx.search.engine.dto.adapters.responses.GoogleResponse;
import com.idexx.search.engine.exception.SearchException;

@Service("googleSearchService")
public class GoogleSearchService implements SearchService {

	@Autowired
	private Adapter<GoogleResponse> googleAdapter;
	
	@Autowired
	private GoogleResponseConverter googleResponseConverter;
	
	@Override
	public List<SearchResultDTO> getResults(String query) throws SearchException {
		GoogleResponse googleResponse = googleAdapter.get(query);
		return googleResponseConverter.convert(googleResponse);
	}
}
