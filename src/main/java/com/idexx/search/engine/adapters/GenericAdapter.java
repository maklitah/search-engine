package com.idexx.search.engine.adapters;

import org.springframework.web.client.RestTemplate;

public class GenericAdapter {
	protected final String URL;
	protected final String limit;
	protected final RestTemplate restTemplate;
	
	public GenericAdapter(RestTemplate restTemplate, String URL, String limit) {
        this.restTemplate = restTemplate;
    	this.URL = URL;
    	this.limit = limit;
    }
}
