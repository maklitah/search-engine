package com.idexx.search.engine.adapters;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.idexx.search.engine.dto.adapters.responses.GoogleResponse;
import com.idexx.search.engine.exception.SearchException;

public class GoogleAdapter extends GenericAdapter implements Adapter<GoogleResponse> {	
	private static String PRINT_TYPE = "books";
	private static String PROJECTION = "lite";
	
    public GoogleAdapter(RestTemplate restTemplate, String URL, String limit) {
        super(restTemplate, URL, limit);
    }

	@Override
	public GoogleResponse get(String query) throws SearchException {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(URL)
		        .queryParam("q", query)
		        .queryParam("maxResults", limit)
		        .queryParam("printType", PRINT_TYPE)
		        .queryParam("projection", PROJECTION);
		
		ResponseEntity<GoogleResponse> googleResponse;
		try {
			googleResponse = restTemplate.getForEntity(uriBuilder.toUriString(), GoogleResponse.class);
		} catch (Exception e) {
			throw new SearchException(HttpStatus.INTERNAL_SERVER_ERROR, "Communication with Google API failed.");
		}
		return googleResponse.getBody();
	}
}
