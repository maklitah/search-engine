package com.idexx.search.engine.adapters;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.idexx.search.engine.dto.adapters.responses.ITunesResponse;
import com.idexx.search.engine.exception.SearchException;

public class ITunesAdapter extends GenericAdapter implements Adapter<ITunesResponse> {
	private static String ENTITY = "album";

    public ITunesAdapter(RestTemplate restTemplate, String URL, String limit) {
    	super(restTemplate, URL, limit);
    }

	@Override
	public ITunesResponse get(String query) throws SearchException {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(URL)
		        .queryParam("term", query)
		        .queryParam("limit", limit)
		        .queryParam("entity", ENTITY);
		
		ResponseEntity<ITunesResponse> iTunesResponse;
		
		try {
			iTunesResponse = restTemplate.getForEntity(uriBuilder.toUriString(), ITunesResponse.class);
		} catch (Exception e) {
			throw new SearchException(HttpStatus.INTERNAL_SERVER_ERROR, "Communication with iTunes API failed.");
		}

		return iTunesResponse.getBody();
	}
}
