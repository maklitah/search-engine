package com.idexx.search.engine.adapters;

import com.idexx.search.engine.exception.SearchException;

public interface Adapter<T> {
	
	T get(String query) throws SearchException;
}
