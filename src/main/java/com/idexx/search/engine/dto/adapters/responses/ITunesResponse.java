package com.idexx.search.engine.dto.adapters.responses;

import java.util.List;

public class ITunesResponse {
	private List<Album> results;
		
	public List<Album> getResults() {
		return results;
	}

	public void setResults(List<Album> results) {
		this.results = results;
	}

	public static class Album {
		private String collectionName;
		private String artistName;
		
		public String getCollectionName() {
			return collectionName;
		}
		
		public void setCollectionName(String collectionName) {
			this.collectionName = collectionName;
		}
		
		public String getArtistName() {
			return artistName;
		}
		
		public void setArtistName(String artistName) {
			this.artistName = artistName;
		}
	}
}
