package com.idexx.search.engine.dto;

import java.io.Serializable;
import java.util.List;

import com.idexx.search.engine.enums.SearchResultType;

public class SearchResultDTO implements Serializable {
	
	private static final long serialVersionUID = -4076093092542784661L;
	
	private String title;
	private SearchResultType type;
	private List<String> creators;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public SearchResultType getSearchResultType() {
		return type;
	}
	public void setSearchResultType(SearchResultType type) {
		this.type = type;
	}
	
	public List<String> getCreators() {
		return creators;
	}
	public void setCreators(List<String> creators) {
		this.creators = creators;
	}
	
	public SearchResultDTO() {
		super();
	}
	
	public SearchResultDTO(String title, SearchResultType type, List<String> creators) {
		super();
		this.title = title;
		this.type = type;
		this.creators = creators;
	}	
}
