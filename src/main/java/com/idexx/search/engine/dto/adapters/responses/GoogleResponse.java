package com.idexx.search.engine.dto.adapters.responses;

import java.util.List;

public class GoogleResponse {
	private List<Book> items;
	
	public List<Book> getItems() {
		return items;
	}

	public void setItems(List<Book> items) {
		this.items = items;
	}

	public static class Book {
		private VolumeInfo volumeInfo;
				
		public VolumeInfo getVolumeInfo() {
			return volumeInfo;
		}

		public void setVolumeInfo(VolumeInfo volumeInfo) {
			this.volumeInfo = volumeInfo;
		}

		public static class VolumeInfo {
			private String title;
			private List<String> authors;
			
			public String getTitle() {
				return title;
			}
			
			public void setTitle(String title) {
				this.title = title;
			}
			
			public List<String> getAuthors() {
				return authors;
			}
			
			public void setAuthors(List<String> authors) {
				this.authors = authors;
			}			
		}
	}
}
