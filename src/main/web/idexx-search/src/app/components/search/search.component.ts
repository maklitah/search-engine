import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../services/search/search.service';
import { Result } from '../../dto/result';
import { BOOK, ALBUM } from '../../enum/type';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
    query: string = '';
    results: Result[];
    type = {book: BOOK, album: ALBUM};

    constructor(private searchService: SearchService) { }

    ngOnInit() {
    }

    getResults(event) {
        this.searchService.search(event).subscribe(response => {
            this.results = response;
        });
    }

    onQueryChange(event) {
        this.query = event;
    }
}
