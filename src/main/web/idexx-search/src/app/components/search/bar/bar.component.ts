import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'search-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.css']
})
export class BarComponent implements OnInit {
    query: string = '';

    @Output()
    queryChange: EventEmitter<string> = new EventEmitter<string>();
    
    @Output()
    buttonClicked: EventEmitter<string> = new EventEmitter<string>();

    constructor() { }

    ngOnInit() {}

    search() {
        this.buttonClicked.emit(this.query);
    }

    onQueryChange(event) {
        this.queryChange.emit(event);
    }
}
