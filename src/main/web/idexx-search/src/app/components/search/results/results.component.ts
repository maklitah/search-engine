import { Component, OnInit, Input } from '@angular/core';
import { Result } from '../../../dto/result';

@Component({
  selector: 'search-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
    @Input() results: Result[];
    @Input() query: string;
    @Input() type;

    constructor() { }

    ngOnInit() {
    }
}
