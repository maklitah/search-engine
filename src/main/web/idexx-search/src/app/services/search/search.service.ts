import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Result } from '../../dto/result';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
    searchUrl: string = '/api/search/';

    constructor(private http: HttpClient) { }

    search(query: string) {
        return this.http.get<Result[]>(this.searchUrl + encodeURIComponent(query));
    }
}
