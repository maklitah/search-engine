export const BOOK = {
    TYPE: "BOOK",
    COLS: ["Books", "Authors"]
};

export const ALBUM = {
    TYPE: "ALBUM",
    COLS: ["Albums", "Artists"]
};