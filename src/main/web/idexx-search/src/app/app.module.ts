import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { AppComponent } from './app.component';
import { SearchComponent } from './components/search/search.component';
import { BarComponent } from './components/search/bar/bar.component';
import { ResultsComponent } from './components/search/results/results.component'

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    BarComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    Ng2SearchPipeModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
