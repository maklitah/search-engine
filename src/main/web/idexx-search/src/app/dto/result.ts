export interface Result {
    title: string;
    searchResultType: string;
    creators: string[];
}