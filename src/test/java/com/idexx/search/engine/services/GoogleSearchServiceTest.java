package com.idexx.search.engine.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.idexx.search.engine.adapters.Adapter;
import com.idexx.search.engine.converters.GoogleResponseConverter;
import com.idexx.search.engine.dto.SearchResultDTO;
import com.idexx.search.engine.dto.adapters.responses.GoogleResponse;
import com.idexx.search.engine.enums.SearchResultType;
import com.idexx.search.engine.exception.SearchException;

@RunWith(SpringRunner.class)
public class GoogleSearchServiceTest {

	@TestConfiguration
    static class GoogleServiceTestContextConfiguration {
  
        @Bean
        public SearchService employeeService() {
            return new GoogleSearchService();
        }
    }
	
	@Autowired
    private SearchService googleSearchService;
 
    @MockBean
    private Adapter<GoogleResponse> googleAdapter;
 
    @MockBean
    private GoogleResponseConverter googleResponseConverter;
    
    private List<SearchResultDTO> results;
    
    @Before
    public void setUp() {
    	results = Stream.of(
    			new SearchResultDTO("title1", SearchResultType.BOOK, new ArrayList<String>()), 
    			new SearchResultDTO("title2", SearchResultType.BOOK, new ArrayList<String>()))
    			.collect(Collectors.toList());
    }
    
    @Test
    public void get() throws SearchException {
    	Mockito.when(googleAdapter.get(Mockito.eq("good job"))).thenReturn(new GoogleResponse());
    	Mockito.when(googleResponseConverter.convert(Mockito.any(GoogleResponse.class))).thenReturn(results);
    	
    	List<SearchResultDTO> actual = googleSearchService.getResults("good job");
    	
    	Assert.assertEquals(2, actual.size());
    	Assert.assertEquals("title1", actual.get(0).getTitle());
    	
    	Mockito.verify(googleAdapter, Mockito.times(1)).get(Mockito.eq("good job"));
    	Mockito.verify(googleResponseConverter, Mockito.times(1)).convert(Mockito.any(GoogleResponse.class));
    }
}
