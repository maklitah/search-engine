package com.idexx.search.engine.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.idexx.search.engine.adapters.Adapter;
import com.idexx.search.engine.converters.ITunesResponseConverter;
import com.idexx.search.engine.dto.SearchResultDTO;
import com.idexx.search.engine.dto.adapters.responses.ITunesResponse;
import com.idexx.search.engine.enums.SearchResultType;
import com.idexx.search.engine.exception.SearchException;

import org.junit.Assert;

@RunWith(SpringRunner.class)
public class ITunesSearchServiceTest {
	
	@TestConfiguration
    static class ITunesServiceTestContextConfiguration {
  
        @Bean
        public SearchService employeeService() {
            return new ITunesSearchService();
        }
    }
	
	@Autowired
    private SearchService iTunesSearchService;
 
    @MockBean
    private Adapter<ITunesResponse> iTunesAdapter;
 
    @MockBean
    private ITunesResponseConverter iTunesResponseConverter;
    
    private List<SearchResultDTO> results;
    
    @Before
    public void setUp() {
    	results = Stream.of(
    			new SearchResultDTO("title1", SearchResultType.ALBUM, new ArrayList<String>()), 
    			new SearchResultDTO("title2", SearchResultType.ALBUM, new ArrayList<String>()))
    			.collect(Collectors.toList());
    }
    
    @Test
    public void get() throws SearchException {
    	Mockito.when(iTunesAdapter.get(Mockito.eq("good job"))).thenReturn(new ITunesResponse());
    	Mockito.when(iTunesResponseConverter.convert(Mockito.any(ITunesResponse.class))).thenReturn(results);
    	
    	List<SearchResultDTO> actual = iTunesSearchService.getResults("good job");
    	
    	Assert.assertEquals(2, actual.size());
    	Assert.assertEquals("title1", actual.get(0).getTitle());
    	
    	Mockito.verify(iTunesAdapter, Mockito.times(1)).get(Mockito.eq("good job"));
    	Mockito.verify(iTunesResponseConverter, Mockito.times(1)).convert(Mockito.any(ITunesResponse.class));
    }
}
