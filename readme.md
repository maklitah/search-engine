# Search Service

## Technology Stack
* Spring Boot
* Angular 8

I chose Angular 8, as I have not used it before (just AngularJS). 
It is an easy-to-grasp-the-concept and ready-to-run framework (Angular CLI is very useful tool), 
and the coding flow from the back-end (in Java and Spring) to the front-end is very smooth
thanks to Typescript, the annotations and the dependency injection concept. Overall, it is wrapped up in Spring Boot,
which builds the Angular Project, which is served as app's main page (localhost:8080). All configuration is exists in pom.xml.
Unit tests for services and integration test are provided in src/test/java package.

## How to run

* Inside the `Assignment` directory execute: mvn clean install. The project will download maven dependencies, but also node.js (frontend-maven-plugin) in order to build the angular project (dist folder). Then, maven-resources-plugin, will copy angular resources to target/classes/public folder. Meanwhile, integration test has run, so asciidoctor-maven-plugin has produced target/generated-docs/api-guide.html for API documentation (Spring REST Docs).
* In the same folder, execute: mvn spring-boot:run. Parameters can be passed for the Google and iTunes API limit, e.g. -Dadapter.google.limit=10 and -Dadapter.itunes.limit=35. Similar to this, APIs can be defined, as adapter.{placeholder}.url.

## Useful URLs
The app will start under localhost:8080. The following Actuator endpoints are enabled:

* Health: GET /actuator/health, returns "UP" when application is up and running.
* Shutdown: POST /actuator/shutdown
* Metrics: GET /actuator/metrics, which returns an array of metrics, that can be used as parameters in URL. For example, considering "system.cpu.usage", /actuator/metrics/system.cpu.usage can be executed.